using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estadopatrulla : MonoBehaviour
{ 
    public Transform[] wayPoints;
    public Color colorEstado = Color.green;

    private Maquinadeestados maquinaDeEstados;
    private Controladorvision controladorVision;
    private ControladorNavMesh controladorNavMesh;
    private int siguienteWayPoint;

    public Vida vidaManager;

    private void Awake()
    {
        controladorVision = GetComponent<Controladorvision>();
        maquinaDeEstados = GetComponent<Maquinadeestados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
    }

    private void Update()
    {
        RaycastHit hit;
        if (controladorVision.puedeVerAlJugador(out hit))
        {
            controladorNavMesh.perseguirObjetivo = hit.transform;
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoPersecucion);
            return;
        }
        if (controladorNavMesh.HemosLlegado())
        {
            siguienteWayPoint = (siguienteWayPoint + 1) % wayPoints.Length;
            controladorNavMesh.ActualizarDestinoNav(wayPoints[siguienteWayPoint].position);
        }

        vidaManager.Huir();
    }
    private void OnEnable()
    {
        maquinaDeEstados.MeshRendererIndicador.material.color = colorEstado;
        actualizarWayPointDestino();
    }

    void actualizarWayPointDestino()
    {
        controladorNavMesh.ActualizarDestinoNav(wayPoints[siguienteWayPoint].position);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && enabled)
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoAlerta);
        }
    }
}
