using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Estadohuida : MonoBehaviour
{
    public Color colorEstado = Color.blue;

    private Maquinadeestados maquinaDeEstados;
    private Controladorvision controladorVision;
    private ControladorNavMesh controladorNavMesh;

    public NavMeshAgent nav;
    public GameObject player;

    private void Awake()
    {
        controladorVision = GetComponent<Controladorvision>();
        maquinaDeEstados = GetComponent<Maquinadeestados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();

    }

    public void Update()
    {
        Vector3 direcBear = transform.position - player.transform.position;
        transform.Translate(direcBear.normalized * nav.speed * Time.deltaTime);
    }

    void OnEnable()
    {
        maquinaDeEstados.MeshRendererIndicador.material.color = colorEstado;
    }
}
