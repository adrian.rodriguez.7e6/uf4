using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estadoalerta : MonoBehaviour
{
    public float velocidadGiroBusqueda = 120f;
    public float duracionBusqueda = 4f;
    public Color colorEstado = Color.yellow;

    private Maquinadeestados maquinaDeEstados;
    private ControladorNavMesh controladorNavMesh;
    private Controladorvision controladorVision;
    private float tiempoBusqueda;
    public Collider player;

    public Vida vidaManager;

    void Awake()
    {
      maquinaDeEstados = GetComponent<Maquinadeestados>();
      controladorNavMesh = GetComponent<ControladorNavMesh>();
      controladorVision = GetComponent<Controladorvision>();
    }

    void OnEnable()
    {
        maquinaDeEstados.MeshRendererIndicador.material.color = colorEstado;
        controladorNavMesh.DetenerNav();
        tiempoBusqueda = 0f;
    }
     void Update()
    {
        RaycastHit hit;
        if (controladorVision.puedeVerAlJugador(out hit))
        {
            controladorNavMesh.perseguirObjetivo = hit.transform;
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoPersecucion);
            return;
        }
        transform.Rotate(0f, velocidadGiroBusqueda * Time.deltaTime, 0f);
        tiempoBusqueda += Time.deltaTime;
        if(tiempoBusqueda >= duracionBusqueda && !OnTriggerEnter(player))
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoPatrulla);
            return;
        }

        vidaManager.Huir();
    }

    public bool OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && enabled)
        {
            return true;
        }
        return false;
    }
}
