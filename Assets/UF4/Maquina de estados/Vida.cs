using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vida : MonoBehaviour
{
    public int vidaOso = 6;
    public int vidaPersonaje = 3;
    private Maquinadeestados maquinaDeEstados;

    private void Awake()
    {
        maquinaDeEstados = GetComponent<Maquinadeestados>();
    }

    public void Update()
    {
        if (vidaOso == 0)
        {
            Destroy(this.gameObject);
        }
        if (vidaPersonaje == 0)
        {
            SceneManager.LoadScene(2);
        }
    }

    public void Huir()
    {
        if (vidaOso == 3)
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.EstadoHuida);
        }
    }


}
