using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controladorvision : MonoBehaviour
{
    public Transform ojos;
    public float rangoVision = 20f;

    private ControladorNavMesh controladorNavMesh;

    private void Awake()
    {
        controladorNavMesh = GetComponent<ControladorNavMesh>();
    }

    public bool puedeVerAlJugador(out RaycastHit hit, bool mirarHacieElJugador = false)
    {
        Vector3 vectorDireccion;
        if (mirarHacieElJugador)
        {
            vectorDireccion = controladorNavMesh.perseguirObjetivo.position - ojos.position;
        }
        else vectorDireccion = ojos.forward;

        return Physics.Raycast(ojos.position, ojos.forward, out hit, rangoVision) && hit.collider.CompareTag("Player");
    }


}
