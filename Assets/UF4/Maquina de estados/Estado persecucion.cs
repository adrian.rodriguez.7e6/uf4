using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estadopersecucion : MonoBehaviour
{
    public Color colorEstado = Color.red;

    private Maquinadeestados maquinaEstados;
    private ControladorNavMesh controladorNavMesh;
    private Controladorvision controladorvision;

    public Vida vidaManager;

    void Awake()
    {
        maquinaEstados = GetComponent<Maquinadeestados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorvision = GetComponent<Controladorvision>();
    }
    void OnEnable()
    {
        maquinaEstados.MeshRendererIndicador.material.color = colorEstado;  
    }
    void Update()
    {
        RaycastHit hit;
        if (!controladorvision.puedeVerAlJugador(out hit, true))
        {
            maquinaEstados.ActivarEstado(maquinaEstados.EstadoAlerta);
            return;
        }


        controladorNavMesh.ActualizarDestinoNav();

        vidaManager.Huir();
    }
}
