using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Daño : MonoBehaviour
{
    public Vida vidaManager;
    public Slider slider;
    
    
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bottle"))
        {
            vidaManager.vidaOso--;
            slider.value--;
            Debug.Log("Oso" + vidaManager.vidaOso);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            vidaManager.vidaPersonaje--;
            Debug.Log("elvis" + vidaManager.vidaPersonaje);
        }
    }
}
